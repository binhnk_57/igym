package com.example.binhnk.igym;

/**
 * Created by binhnk on 03/10/2016.
 */

public class Constants {

    public static abstract class MuscleConstants {

        public static final String TABLE_NAME = "muscles_table";
        public static final String NAME = "name";
        public static final String IMAGE = "image";
    }
}
