package com.example.binhnk.igym;

import android.app.Fragment;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.binhnk.igym.database.MusclesDatabaseHelper;
import com.example.binhnk.igym.fragment.BMIDialogFragment;
import com.example.binhnk.igym.muscles.fragment.MuscleFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private boolean doubleBackToExitPressedOnce = false;
String TAG = "aa";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

//        Set MuscleFragment is default fragment
        android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, new MuscleFragment()).commit();
        navigationView.getMenu().getItem(0).setChecked(true);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));

        importMuscleDatabase(this);

        FloatingActionButton fabBMI = (FloatingActionButton) findViewById(R.id.bmi_cal);
        fabBMI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BMIDialogFragment bmiDialog = new BMIDialogFragment();
                bmiDialog.show(getFragmentManager(), TAG);
//                Toast.makeText(MainActivity.this, "BMI Calculator", Toast.LENGTH_SHORT).show();
            }
        });

        FloatingActionButton fabTimer = (FloatingActionButton) findViewById(R.id.timer);
        fabTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Timer", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Press BACK again to exit app", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        ActionBar actionBar = getSupportActionBar();
        Fragment fragment = null;

        switch (id) {
            case R.id.nav_exercise:
                fragment = new MuscleFragment();
                actionBar.setTitle("Exercise");
                break;
            case R.id.nav_diary:
                actionBar.setTitle("Exercise diary");
                Toast.makeText(MainActivity.this, "Diary", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_utilities:
                actionBar.setTitle("Utilities");
                Toast.makeText(MainActivity.this, "Utilities", Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_gym_room:
                actionBar.setTitle("Gym room");
                Toast.makeText(MainActivity.this, "Gym room", Toast.LENGTH_SHORT).show();
                break;
            default:

                break;
        }

        if (fragment != null) {
            android.app.FragmentManager manager = getFragmentManager();
            manager.beginTransaction().replace(R.id.content_main, fragment).commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void importMuscleDatabase(Context context) {
        MusclesDatabaseHelper musclesDatabaseHelper = new MusclesDatabaseHelper(context);
        SQLiteDatabase db = musclesDatabaseHelper.getReadableDatabase();
        musclesDatabaseHelper.deleteAllRow(db);

        SQLiteDatabase db1 = musclesDatabaseHelper.getWritableDatabase();
        musclesDatabaseHelper.addOneRow(db1, "Abs muscles", Utils.bitmapToString(BitmapFactory.decodeResource(getResources(), R.drawable.muscles_abs)));
        musclesDatabaseHelper.addOneRow(db1, "Arm muscles", Utils.bitmapToString(BitmapFactory.decodeResource(getResources(), R.drawable.muscles_arm)));
        musclesDatabaseHelper.addOneRow(db1, "Chest muscles", Utils.bitmapToString(BitmapFactory.decodeResource(getResources(), R.drawable.muscles_chest)));
        musclesDatabaseHelper.addOneRow(db1, "Lats muscles", Utils.bitmapToString(BitmapFactory.decodeResource(getResources(), R.drawable.muscles_lats)));

        db.close();
        db1.close();
    }


}
