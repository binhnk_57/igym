package com.example.binhnk.igym.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.binhnk.igym.R;
import com.example.binhnk.igym.muscles.fragment.AbsExerciseFragment;
import com.example.binhnk.igym.objects.AbsExerciseObject;

import java.util.ArrayList;

/**
 * Created by binhnk on 04/10/2016.
 */

public class AbsExerciseActivity extends AppCompatActivity {

    RecyclerView recyclerViewAbsExercise;
    RecyclerView.Adapter adapterAbsExercise;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<AbsExerciseObject> absExerciseList = new ArrayList<>();
    Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_abs_muscles);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);

        absExerciseList.add(new AbsExerciseObject("Criss cross", BitmapFactory.decodeResource(getResources(), R.drawable.abs_crisscross)));
        absExerciseList.add(new AbsExerciseObject("Crossover Crunch", BitmapFactory.decodeResource(getResources(), R.drawable.abs_crossover_crunch)));
        absExerciseList.add(new AbsExerciseObject("Crunch", BitmapFactory.decodeResource(getResources(), R.drawable.abs_crunch)));
        absExerciseList.add(new AbsExerciseObject("Flutter Kicks", BitmapFactory.decodeResource(getResources(), R.drawable.abs_flutter_kicks)));
        absExerciseList.add(new AbsExerciseObject("Legs Lower", BitmapFactory.decodeResource(getResources(), R.drawable.abs_legs_lower)));

        recyclerViewAbsExercise = (RecyclerView) findViewById(R.id.recyclerViewAbsExercise);
        layoutManager = new LinearLayoutManager(context);
        recyclerViewAbsExercise.setLayoutManager(layoutManager);
        recyclerViewAbsExercise.setHasFixedSize(true);

        adapterAbsExercise = new AdapterAbsExercise(absExerciseList, context);
        recyclerViewAbsExercise.setAdapter(adapterAbsExercise);
    }

//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_abs_muscles, container, false);
//
////        android.app.ActionBar actionBar = getActivity().getActionBar();
////        actionBar.setTitle("Abs Exercise");
//        context = getActivity().getApplicationContext();
//
//        absExerciseList.add(new AbsExerciseObject("Criss cross", BitmapFactory.decodeResource(getResources(), R.drawable.abs_crisscross)));
//        absExerciseList.add(new AbsExerciseObject("Crossover Crunch", BitmapFactory.decodeResource(getResources(), R.drawable.abs_crossover_crunch)));
//        absExerciseList.add(new AbsExerciseObject("Crunch", BitmapFactory.decodeResource(getResources(), R.drawable.abs_crunch)));
//        absExerciseList.add(new AbsExerciseObject("Flutter Kicks", BitmapFactory.decodeResource(getResources(), R.drawable.abs_flutter_kicks)));
//        absExerciseList.add(new AbsExerciseObject("Legs Lower", BitmapFactory.decodeResource(getResources(), R.drawable.abs_legs_lower)));
//
//        recyclerViewAbsExercise = (RecyclerView) view.findViewById(R.id.recyclerViewAbsExercise);
//        layoutManager = new LinearLayoutManager(context);
//        recyclerViewAbsExercise.setLayoutManager(layoutManager);
//        recyclerViewAbsExercise.setHasFixedSize(true);
//
//        adapterAbsExercise = new AdapterAbsExercise(absExerciseList, context);
//        recyclerViewAbsExercise.setAdapter(adapterAbsExercise);
//        return view;
//    }


    private class AdapterAbsExercise extends RecyclerView.Adapter<ViewHolder> {

        private ArrayList<AbsExerciseObject> list = new ArrayList<>();
        Context context;

        public AdapterAbsExercise(ArrayList<AbsExerciseObject> list, Context context) {
            this.list = list;
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_abs_exercise, parent, false);
            return new ViewHolder(view, context, list);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.imageViewAbsExercise.setImageBitmap(list.get(position).getAbsExercisePicture());
            holder.textViewAbsExercise.setText(list.get(position).getAbsExerciseName());
        }

        @Override
        public int getItemCount() {
            return absExerciseList.size();
        }
    }

    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageViewAbsExercise;
        private TextView textViewAbsExercise;
        ArrayList<AbsExerciseObject> arrayList = new ArrayList<>();
        Context context;

        public ViewHolder(View itemView, Context context, ArrayList<AbsExerciseObject> list) {
            super(itemView);
            this.context = context;
            this.arrayList = list;

            itemView.setOnClickListener(this);

            imageViewAbsExercise = (ImageView) itemView.findViewById(R.id.imageViewAbsExercise);
            textViewAbsExercise = (TextView) itemView.findViewById(R.id.textViewAbsExercise);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();

            switch (position) {
                case 0:
                    Intent intent = new Intent(getApplicationContext(), AbsExerciseFragment.class);
                    startActivity(intent);
                    break;
                case 1:
                    Toast.makeText(getApplicationContext(), "Crossover Crunch", Toast.LENGTH_SHORT).show();
                    break;
                case 2:
                    Toast.makeText(getApplicationContext(), "Crunch", Toast.LENGTH_SHORT).show();
                    break;
                case 3:
                    Toast.makeText(getApplicationContext(), "Flutter Kicks", Toast.LENGTH_SHORT).show();
                    break;
                case 4:
                    Toast.makeText(getApplicationContext(), "Legs lower", Toast.LENGTH_SHORT).show();
                    break;

                default:
                    break;

            }
        }
    }
}
