package com.example.binhnk.igym.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.binhnk.igym.MainActivity;
import com.example.binhnk.igym.R;

/**
 * Created by binhnk on 04/10/2016.
 */

public class LoadingScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading_screen);

        Thread wellcomeThread = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent i = new Intent(LoadingScreen.this, MainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };

        wellcomeThread.start();
    }
}
