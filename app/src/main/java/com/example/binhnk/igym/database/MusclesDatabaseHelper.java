package com.example.binhnk.igym.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.binhnk.igym.Constants;

import static com.example.binhnk.igym.Constants.MuscleConstants.TABLE_NAME;

/**
 * Created by binhnk on 03/10/2016.
 */

public class MusclesDatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "iGym.db";
    private static final int VERSION = 1;

    private static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
            " (" + Constants.MuscleConstants.NAME + " VARCHAR NOT NULL, " +
            Constants.MuscleConstants.IMAGE + " VARCHAR NOT NULL);";

    private static final String TAG = "MusclesDatabaseHelper";

    public MusclesDatabaseHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public void addOneRow(SQLiteDatabase db, String name, String image) {
        db.execSQL(CREATE_TABLE);

        ContentValues contentValues = new ContentValues();
        contentValues.put(Constants.MuscleConstants.NAME, name);
        contentValues.put(Constants.MuscleConstants.IMAGE, image);

        db.insert(TABLE_NAME, null, contentValues);
        Log.d(TAG, "One row inserted..");
    }

    public Cursor getInfo(SQLiteDatabase db) {
        String[] projection = {Constants.MuscleConstants.NAME, Constants.MuscleConstants.IMAGE};
        Cursor cursor = db.query(Constants.MuscleConstants.TABLE_NAME, projection, null, null, null, null, null);

        return cursor;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void deleteAllRow(SQLiteDatabase db) {
        db.execSQL("DELETE FROM " + Constants.MuscleConstants.TABLE_NAME + ";");
    }
}
