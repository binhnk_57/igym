package com.example.binhnk.igym.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.binhnk.igym.R;
import com.example.binhnk.igym.Utils;

/**
 * Created by binhnk on 04/10/2016.
 */

public class BMIDialogFragment extends DialogFragment {

    LayoutInflater inflater;
    View view;

    EditText edtHeight, edtWeight;
//    TextView tvResult;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        inflater = getActivity().getLayoutInflater();

        view = inflater.inflate(R.layout.bmi_cal_dialog_background, null);

        edtHeight = (EditText) view.findViewById(R.id.height);
        edtWeight = (EditText) view.findViewById(R.id.weight);
//        tvResult = (TextView) view.findViewById(R.id.bmiResult);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view).setPositiveButton("EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setNegativeButton("CALCULATE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!edtHeight.getText().toString().equals("") || !edtWeight.getText().toString().equals("")) {
                    double result = Utils.bmiCalculator(Double.parseDouble(edtHeight.getText().toString()), Double.parseDouble(edtWeight.getText().toString()));
                    result = Math.round(result*100)/100;
                    String s = "";
                    if (0 < result && result < 18.5) {
                        s = "Your BMI: " + String.valueOf(result) + " - Underweight";
                    } else if (18.5 <= result & result <= 24.9) {
                        s = "Your BMI: " + String.valueOf(result) + " - Normal weight";
                    } else if (25.0 <= result & result <= 29.9) {
                        s = "Your BMI: " + String.valueOf(result) + " - Overweight";
                    } else if (30.0 <= result) {
                        s = "Your BMI: " + String.valueOf(result) + " - Obese";
                    }
                    Toast.makeText(getActivity().getApplicationContext(), s, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Cannot access null value", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return builder.create();
    }
}
