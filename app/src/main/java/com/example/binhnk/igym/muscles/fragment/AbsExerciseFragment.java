package com.example.binhnk.igym.muscles.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.binhnk.igym.R;

/**
 * Created by binhnk on 04/10/2016.
 */

public class AbsExerciseFragment extends AppCompatActivity {
    static boolean showVideo = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_abs_exercise);
        final ImageView imgvExercise = (ImageView) findViewById(R.id.imageViewExercise);
        final TextView tvContentExercise = (TextView) findViewById(R.id.textViewContentExercise);
        final ImageView imgvShowHideVideo = (ImageView) findViewById(R.id.imageViewShowHideVideo);

        final VideoView videoView = (VideoView) findViewById(R.id.videoView);

        imgvShowHideVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showVideo == false) {
                    videoView.setVisibility(View.VISIBLE);
                    imgvShowHideVideo.setImageResource(R.drawable.ic_arrow_drop_up);
                    showVideo = true;
                } else {
                    videoView.setVisibility(View.GONE);
                    imgvShowHideVideo.setImageResource(R.drawable.ic_arrow_drop_down);
                    showVideo = false;
                }
            }
        });

    }
//
//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_abs_exercise, container, false);
//
//        final ImageView imgvExercise = (ImageView) view.findViewById(R.id.imageViewExercise);
//        final TextView tvContentExercise = (TextView) view.findViewById(R.id.textViewContentExercise);
//        final ImageView imgvShowHideVideo = (ImageView) view.findViewById(R.id.imageViewShowHideVideo);
//
//        final VideoView videoView = (VideoView) view.findViewById(R.id.videoView);
//
//        imgvShowHideVideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (showVideo == false) {
//                    videoView.setVisibility(View.VISIBLE);
//                    imgvShowHideVideo.setImageResource(R.drawable.ic_arrow_drop_up);
//                    showVideo = true;
//                } else {
//                    videoView.setVisibility(View.GONE);
//                    imgvShowHideVideo.setImageResource(R.drawable.ic_arrow_drop_down);
//                    showVideo = false;
//                }
//            }
//        });
//
//        return view;
//
//    }
}
