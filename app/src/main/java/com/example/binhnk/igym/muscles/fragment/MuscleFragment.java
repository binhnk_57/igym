package com.example.binhnk.igym.muscles.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.binhnk.igym.R;
import com.example.binhnk.igym.Utils;
import com.example.binhnk.igym.activity.AbsExerciseActivity;
import com.example.binhnk.igym.database.MusclesDatabaseHelper;
import com.example.binhnk.igym.objects.MusclesObject;

import java.util.ArrayList;

/**
 * Created by binhnk on 30/09/2016.
 */

public class MuscleFragment extends Fragment {

    RecyclerView musclesRecyclerView;
    RecyclerView.Adapter musclesAdapter;
    RecyclerView.LayoutManager layoutManager;

    ArrayList<MusclesObject> musclesObjectList = new ArrayList<>();

    Context muscleCtx;
    MusclesDatabaseHelper musclesDatabaseHelper;
    SQLiteDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_muscles, container, false);

        muscleCtx = getActivity().getApplicationContext();
        musclesDatabaseHelper = new MusclesDatabaseHelper(muscleCtx);

        musclesRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewMuscles);
        layoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        musclesRecyclerView.setLayoutManager(layoutManager);
        musclesRecyclerView.setHasFixedSize(true);

        if (musclesDatabaseHelper != null) {
            db = musclesDatabaseHelper.getReadableDatabase();
            Cursor cursor = musclesDatabaseHelper.getInfo(db);

            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                do {
                    MusclesObject musclesObject = new MusclesObject(cursor);
                    musclesObjectList.add(musclesObject);
                } while (cursor.moveToNext());
            }
        }

        musclesAdapter = new MusclesAdapter(musclesObjectList, getActivity().getApplicationContext());
        musclesRecyclerView.setAdapter(musclesAdapter);

        return view;
    }

    class MusclesAdapter extends RecyclerView.Adapter<MusclesAdapter.ViewHolder> {

        private ArrayList<MusclesObject> musclesObjectList = new ArrayList<>();
        private Context context;

        public MusclesAdapter(ArrayList<MusclesObject> musclesObjectList, Context context) {
            this.musclesObjectList = musclesObjectList;
            this.context = context;
        }

        @Override
        public MusclesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_item_muscles, parent, false);
            return new MusclesAdapter.ViewHolder(view, context, musclesObjectList);
        }

        @Override
        public void onBindViewHolder(MusclesAdapter.ViewHolder holder, int position) {
            holder.imgvMuscle.setImageBitmap(Utils.stringToBitmap(musclesObjectList.get(position).getMuscleImage()));
            holder.tvMuscle.setText(musclesObjectList.get(position).getMuscleName());
        }

        @Override
        public int getItemCount() {
            return musclesObjectList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            private CardView cardViewMuscle;
            private ImageView imgvMuscle;
            private TextView tvMuscle;
            ArrayList<MusclesObject> arrayList = new ArrayList<>();
            Context context;

            public ViewHolder(View itemView, Context context, ArrayList<MusclesObject> arrayList) {
                super(itemView);
                this.context = context;
                this.arrayList = arrayList;

                itemView.setOnClickListener(this);

                cardViewMuscle = (CardView) itemView.findViewById(R.id.cardViewMuscles);
                imgvMuscle = (ImageView) itemView.findViewById(R.id.imageViewMuscle);
                tvMuscle = (TextView) itemView.findViewById(R.id.textViewMuscle);
            }

            @Override
            public void onClick(View v) {
                int position = getAdapterPosition();
                switch (position) {
                    case 0:
                        Intent intent = new Intent(context, AbsExerciseActivity.class);
                        startActivity(intent);
                        break;
                    case 1:
                        Toast.makeText(context, "Arm Muscles Exercise", Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        Toast.makeText(context, "Chest muscles Exercise", Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        Toast.makeText(context, "Lats Muscles Exercise", Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
