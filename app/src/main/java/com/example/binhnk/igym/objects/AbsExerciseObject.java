package com.example.binhnk.igym.objects;

import android.graphics.Bitmap;

/**
 * Created by binhnk on 04/10/2016.
 */

public class AbsExerciseObject {

    private String absExerciseName;
    private Bitmap absExercisePicture;

    public AbsExerciseObject(String absExerciseName, Bitmap absExercisePicture) {
        this.absExerciseName = absExerciseName;
        this.absExercisePicture = absExercisePicture;
    }

    public String getAbsExerciseName() {
        return absExerciseName;
    }

    public void setAbsExerciseName(String absExerciseName) {
        this.absExerciseName = absExerciseName;
    }

    public Bitmap getAbsExercisePicture() {
        return absExercisePicture;
    }

    public void setAbsExercisePicture(Bitmap absExercisePicture) {
        this.absExercisePicture = absExercisePicture;
    }
}
