package com.example.binhnk.igym.objects;

import android.database.Cursor;

/**
 * Created by binhnk on 30/09/2016.
 */

public class MusclesObject {
    private String muscleName;
    private String muscleImage;

    public MusclesObject(String muscleName, String muscleImage) {
        this.muscleName = muscleName;
        this.muscleImage = muscleImage;
    }

    public MusclesObject(Cursor cursor) {
        this.muscleName = cursor.getString(0);
        this.muscleImage = cursor.getString(1);
    }

    public String getMuscleName() {
        return muscleName;
    }

    public void setMuscleName(String muscleName) {
        this.muscleName = muscleName;
    }

    public String getMuscleImage() {
        return muscleImage;
    }

    public void setMuscleImage(String muscleImage) {
        this.muscleImage = muscleImage;
    }
}
